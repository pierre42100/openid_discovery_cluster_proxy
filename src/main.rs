use actix_web::{App, HttpResponse, HttpServer, web};
use actix_web::dev::HttpResponseBuilder;
use log::*;

#[derive(Debug, Clone)]
struct Config {
    /// Where the real OpenID discovery service can be reached
    upstream_addr: String,

    /// The hostname to specify in the headers of the request to the upstream service
    upstream_hostname: String,

    /// The address this server must listen on
    listen_addr: String,

    /// The URL where the upstream service can be reached outside of the cluster
    public_url: String,
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    env_logger::init();

    let config = Config {
        upstream_addr: std::env::var("UPSTREAM_ADDR").unwrap(),
        upstream_hostname: std::env::var("UPSTREAM_HOSTNAME").unwrap(),
        listen_addr: std::env::var("LISTEN_ADDR").unwrap(),
        public_url: std::env::var("PUBLIC_URL").unwrap(),
    };

    println!("{:#?}", config);

    let config_clone = config.clone();

    HttpServer::new(move || {
        let config_clone = config_clone.clone();
        App::new()
            .data(config_clone)
            .route("**", web::to(proxy_request))
    })
        .bind(&config.listen_addr)?
        .run()
        .await
}

async fn proxy_request(req: web::HttpRequest, config: web::Data<Config>) -> std::io::Result<HttpResponse> {
    let url = format!("{}{}", config.upstream_addr, req.uri());
    debug!("Proxify {}", url);

    let response = reqwest::blocking::Client::new().get(url)
        .header("Host", &config.upstream_hostname)
        .send();

    let response = match response {
        Ok(r) => r,
        Err(e) => {
            log::error!("Proxy request failed! {:#?}", e);
            return Ok(HttpResponse::InternalServerError().body("Failed to proxy request!"));
        }
    };

    let status = response.status();
    let content_type = String::from_utf8_lossy(
        response.headers()["content-type"].as_bytes()).to_string();

    let mut content = match response.bytes() {
        Ok(c) => c.to_vec(),
        Err(e) => {
            log::error!("Failed to read upstream response! {:#?}", e);
            return Ok(HttpResponse::InternalServerError().body("Failed to read upstream response!"));
        }
    };


    // Modify reponse if required
    if content_type.eq("application/json") {
        let mut v: serde_json::Value = serde_json::from_slice(content.as_ref())?;
        if let Some(obj) = v.as_object_mut() {
            adapt_value(&config, "issuer", obj);
            adapt_value(&config, "authorization_endpoint", obj);
            adapt_value(&config, "end_session_endpoint", obj);
            adapt_value(&config, "check_session_iframe", obj);
        }


        content = serde_json::to_vec(&v)?;
    }

    Ok(HttpResponseBuilder::new(status)
        .header("Content-Type", content_type)
        .body(content))
}

fn adapt_value(c: &Config, key: &str, obj: &mut serde_json::Map<String, serde_json::Value>) {
    let mut new_val = None;
    if let Some(val) = obj.get(key) {
        if let Some(s) = val.as_str() {
            new_val = Some(adapt_url(c, s));
        }
    }

    if let Some(val) = new_val {
        obj.insert(key.to_string(), serde_json::Value::from(val));
    }
}

fn adapt_url(c: &Config, val: &str) -> String {
    let split = val.splitn(4, "/").skip(3).next();

    match split {
        None => val.to_string(),
        Some(uri) => format!("{}/{}", c.public_url, uri)
    }
}