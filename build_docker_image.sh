#!/bin/bash
cargo build --release

rm -rf image
mkdir image
cp target/release/openid-discovery-cluster-proxy image

docker build -f Dockerfile image -t pierre42100/openid-discovery-cluster-proxy

rm -rf image