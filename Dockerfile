FROM debian:bullseye-slim

ENV "UPSTREAM_ADDR" "http://upstream:1234"
ENV "UPSTREAM_HOSTNAME" "myprov"
ENV "LISTEN_ADDR" "0.0.0.0:3456"
ENV "PUBLIC_URL" "https://myprov.com"

COPY openid-discovery-cluster-proxy /usr/local/bin/openid-discovery-cluster-proxy

ENTRYPOINT /usr/local/bin/openid-discovery-cluster-proxy
